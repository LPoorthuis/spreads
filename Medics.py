# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 21:19:28 2014

@author: lukas
"""

class Medic (object):
    experience = 0.1
    radius = 3
    values = []
    
    def __init__(self,x ,y, status):
        self.status = status
        self.time = 0
        self.x = x
        self.y = y
        
    def __repr__(self):
        return 'M'
        
    def turn(self):
        self.values.append(self.status)
        self.time += 1
        self.experience += 0.1
        return 0

    def getInfo(self):
        field = [[0 for i in range(10)] for j in range(10)]
        
        list = [[self.status, self.time, self.experience, self.radius], field]
        
        return list
    
    def info(self):
        str  = "Medic\n"
        str += "time       : %s\n" % self.time
        str += "status     : %s\n" % self.status
        str += "radius     : %s\n" % self.radius
        str += "experience : %s" % self.experience
        return str