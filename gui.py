# -*- coding: utf-8 -*-

import sys
sys.path.append('/home/lukas/workspacePY/Spreads/perlin-noise')

from Map import Map
from fun import game
from Zombie import Zombie
from Infected import Infected
from Medics import Medic
from Civil import Civil

import Tkinter
import ttk
import Image
import matplotlib.pyplot as plt

#img Dummy
civImg = None
medImg = None
zomImg = None
infImg = None
whiImg = None
gamImg = None

#color list
colorRGB = [[255, 0, 0],   # red
            [255, 255, 0], # yellow
            [128, 128, 0], # olive
            [0, 255, 0],   # lime
            [0, 128, 128], # teal
            [0, 255, 255], # cyan
            [0, 128, 0],   # green
            [0, 0, 255],   # blue
            [128, 0, 128], # purple
            [255, 0, 255]] # magenta

#unicolored Img
img = [0 for i in range(len(colorRGB))]

#attributes
step = 30
width = 32
height = 32

#list dummy
x = [step*i for i in range(step)]
y = x
perlin = None

def bluePrint(color, step, i):
    dummyPh = Tkinter.PhotoImage(width=step, height=step)
    r,g,b = color
    width = dummyPh.width()
    height = dummyPh.height()
    hexcode = "#%02x%02x%02x" % (color[0], color[1], color[2])
    horizontal_line = "{" + " ".join([hexcode]*width) + "}"
    dummyPh.put(" ".join([horizontal_line]*height))
    return dummyPh

def createImg():
    global civImg, medImg, zomImg, infImg, whiImg, gamImg
    global sample1, sample2, sample3, sample4, sample5, sample6, sample7
    global sample8, sample9, sample10, img, colorRGB
    #images
    civImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/a6_30.gif")
    medImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/medical77_30.gif")
    zomImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/zombie1_30.gif")
    infImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/virus9_30.gif")
    whiImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/white_30.gif")
    gamImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/game.gif")
    
    for i in range(len(img)):
        img[i]  = bluePrint(colorRGB[i], step, 1)

def fill(field):
    global colorRGB, x, y, step, width, height, cv
    photo = Tkinter.PhotoImage(width=width*step, height=height*step)
    loUp = [i*1./10 for i in range(0, 11)]
    
    for i in range(len(field)):
        for j in range(len(field[i])):
            for l in range(len(loUp) - 1):
                if field[i][j] <  loUp[l+1] and loUp[l] <= field[i][j]:
                    cv.create_image(step*i, step*j, anchor=Tkinter.NW, image=img[l])
                    continue

    photo.write('/home/lukas/workspacePY/Spreads/png/game.gif', format='gif')

# info on object in field [i][j]
def onObjectClick(event, i, j):
    global spread
    print 'click'
        
    tmpY = spread.field[i][j].values
    tmpX = [x for x in range(len(tmpY))]
    print tmpX
    print tmpY
    plt.plot(tmpX, tmpY)
    plt.savefig('stats/status%s%s.jpg'%(i,j))
    #Image.open('stats/status'+str(i)+str(j)+'.jpg').save('stats/status'+str(i)+str(j)+'.gif')
    
    print 'i=' + str(i) + ' j=' + str(j)
    tmp = Tkinter.Toplevel(root)
    tmp.title("Infobox for %s at (%s,%s)" % (spread.field[i][j].__class__.__name__, i, j))
    tabs = ttk.Notebook(tmp)
    
    content = Tkinter.Frame(tabs)
    seed = Tkinter.Frame(tabs)
    #graph = Tkinter.Frame(tabs)

    contentLabel = Tkinter.Label(content, text=spread.field[i][j].info(), height=10, width=50)
    contentLabel.pack()
    
    seedLabel = Tkinter.Label(seed, text="seed", height=5, width=25)
    seedLabel.pack()
    
    #tmpCV = Tkinter.Canvas(graph)
    #tmpCV.create_image(0, 0, anchor=Tkinter.NW, image="stats/status%s%s.gif"%(i,j))
    #tmpCV.pack()

    tabs.add(content, text="Object")
    tabs.add(seed, text="Seed")
    #tabs.add(graph, text="Graph")
    
    tabs.pack()

def plotImage(i, j, img, identTag):
    cv.tag_bind(cv.create_image(j*30, i*30, image=img, anchor='nw', tags=identTag), "<ButtonPress-1>", lambda event, l=i, m=j: onObjectClick(event, l, m))
    cv.pack()

def updateField():
    global perlin
    fill(perlin)
    cv.create_image(0, 0, image=gamImg, anchor='nw')
    cv.pack()
    for i in range(len(spread.field)):
        for j in range(len(spread.field[i])):
            if type(spread.field[i][j]) is Zombie:
                plotImage(i, j, zomImg, 'zomTag')
            if type(spread.field[i][j]) is Civil:
                plotImage(i, j, civImg, 'civTag')
            if type(spread.field[i][j]) is Medic:
                plotImage(i, j, medImg, 'medTag')
            if type(spread.field[i][j]) is Infected:
                plotImage(i, j, infImg, 'infTag')

def startup():
    for i in range(len(spread.field)):
        for j in range(len(spread.field[i])):
            tag = str(i) + str(j)
            cv.tag_bind(cv.create_image(i*30, j*30, image=whiImg, anchor='nw', tags=tag), "<ButtonPress-1>", lambda event, l=i, m=j: onObjectClick(event, l, m))

    cv.pack()
    
def turn():
    global i, T
    i += 1
    T.delete(1.0, Tkinter.END)
    T.insert(Tkinter.END, "turn: " + str(i))
    spread.turn()
    cv.delete("all")
    updateField()
        
def spiel():
    global spread
    cv.delete("all")

    spread.initGame()
    updateField()
    
def bg():
    global gamImg
    gamImg = Tkinter.PhotoImage(file="/home/lukas/workspacePY/Spreads/png/game.gif")
    cv.create_image(0, 0, image=gamImg, anchor='nw')
    cv.pack()
    
def wbg():
    for i in range(len(spread.field)):
        for j in range(len(spread.field[i])):
            plotImage(i, j, whiImg, 'whiTag')
    cv.pack()
    
def mapGen():
    global perlin
    perlin = Map(width, height, 30)
    perlin = perlin.create()
    print 'map created -> now filling visual'
    fill(perlin)
    print 'filled visual'
    bg()
    
def fillCanvas():
    #need to think about why i put this here
    return None
    
def reset():
    global i, spread, T
    i = 0
    T.delete(1.0, Tkinter.END)
    T.insert(Tkinter.END, "turn: " + str(i))
    cv.delete("all")
    wbg()

if __name__ == '__main__':
    root = Tkinter.Tk()
    
    createImg()

    root.minsize(step*width, step*height+25)
    #root.resizable(width=False, height=False)
    
    cv = Tkinter.Canvas()
    cv.pack(side='top', fill='both', expand='yes')
    
    spread = game(width, height)
    i = 0

    #title
    root.title("Spreads")
    
    #attributes
    map = 0
    
    #buttons for game control
    start = Tkinter.Button(root, text='start', command=spiel)
    start.pack(side="left")
    # add exception catch after reset. set i = 0
    # AttributeError: 'int' object has no attribute 'turn'
    turn = Tkinter.Button(root, text='turn', command=turn)
    turn.pack(side="left")
    turn = Tkinter.Button(root, text='mapGen', command=mapGen)
    turn.pack(side="left")
    turn = Tkinter.Button(root, text='reset', command=reset)
    turn.pack(side="left")
    
    T = Tkinter.Text(root, height=1, width=10)
    T.pack()
    T.insert(Tkinter.END, "turn: " + str(i))
    T.pack(side="right")
    
    startup()
    
    #cv.tag_bind('zomTag', '<ButtonPress-1>', onObjectClick)
    #cv.tag_bind('infTag', '<ButtonPress-1>', onObjectClick)
    #cv.tag_bind('medTag', '<ButtonPress-1>', onObjectClick)
    #cv.tag_bind('civTag', '<ButtonPress-1>', onObjectClick)

    cv.pack()
    
    root.mainloop()
