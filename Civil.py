# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 21:06:23 2014

@author: lukas
"""

class Civil (object):
    resistance = 0
    values = []
    
    def __init__(self, x, y, status):
        self.status = status
        self.time = 0
        self.x = x
        self.y = y
        
    def __repr__(self):
        return 'C'
        
    def turn(self):
        self.values.append(self.status)
        self.time += 1
        if not self.change(0) == 0:
            return self.change(0)
        return 0
        
    def change(self, value):
        self.status += value
        
        if self.status < -7:
            return 1
        return 0

    def getInfo(self):
        field = [[0 for i in range(10)] for j in range(10)]
        
        list = [[self.status, self.time, self.resistance], field]
        
        return list
        
    def info(self):
        str  = "Civil\n"
        str += "time       : %s\n" % self.time
        str += "status     : %s\n" % self.status
        str += "resistance : %s" % self.resistance
        return str