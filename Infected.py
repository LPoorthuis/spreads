# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 21:18:39 2014

@author: lukas
"""

class Infected (object):
    values = []    
    
    def __init__(self, x, y, status):
        self.status = status
        self.time = 0
        self.x = x 
        self.y = y 
        
    def __repr__(self):
        return 'I'
        
    def turn(self):
        self.values.append(self.status)
        self.time += 1
        self.status -= 1
        
        if self.status < -5:
            return 1
        if self.status > 5:
            return 2

        return 0
        
    def getInfo(self):
        field = [[0 for i in range(10)] for j in range(10)]
        
        list = [[self.status, self.time], field]
        
        return list
    
    def info(self):
        str  = "Infected\n"
        str += "time   : %s\n" % self.time
        str += "status : %s" % self.status
        return str