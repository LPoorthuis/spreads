# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 22:10:58 2014

@author: lukas
"""

class Zombie (object):
    probDirect = 0.1
    radius = 2
    toxic = 2
    values = []
    
    def __init__(self, x, y, status):
        self.status = status
        self.time = 0
        self.x = x
        self.y = y
        
    def __repr__(self):
        return 'Z'
        
    def bite(self):
        return self.toxic
        
    def turn(self):
        self.values.append(self.status)
        self.time += 1
        if self.status > 10:
            return 2
        return 0

    def getInfo(self):
        field = [[0 for i in range(10)] for j in range(10)]
        
        list = [[self.status, self.time, self.toxic, self.toxic, self.probDirect], field]
        
        return list
    
    def info(self):
        str  = "Zombie\n"
        str += "time : %s\n" % self.time
        str += "status : %s\n" % self.status
        str += "radius : %s\n" % self.radius
        str += "toxicity : %s\n" % self.toxic
        str += "crit. Bite : %s" % self.probDirect
        return str