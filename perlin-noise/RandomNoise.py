# -*- coding: utf-8 -*-

import random

class RandomNoise:
    
    def interpolate(self, x0, x1, alpha):
        return x0 * (1 - alpha) + alpha * x1    
    
    def generateWhiteNoise(self, width, height):
        field = [[0 for i in range(width)] for j in range(height)]
        
        for i in range(width):
            for j in range(height):
                field[i][j] = random.random()
                
        return field
        
    def generateSmoothNoise(self, field, octave):
        width = len(field)
        height = len(field[0])
        
        res = [[0 for i in range(width)] for j in range(height)]
        
        samplePeriod = 1 << octave # 2 ^ octave
        sampleFreq = 1. / samplePeriod
        
        for i in range(width):
            sample_i0 = (i / samplePeriod) * samplePeriod
            sample_i1 = (sample_i0 + samplePeriod) % width
            
            horizontal_blend = (i - sample_i0) * sampleFreq
            
            for j in range(height):
                sample_j0 = (j / samplePeriod) * samplePeriod
                sample_j1 = (sample_j0 + samplePeriod) % height
                
                vertical_blend = (j - sample_j0) * sampleFreq
                
                # blend top two corners
                top = self.interpolate(field[sample_i0][sample_j0], field[sample_i1][sample_j0], horizontal_blend)
				
                # blend bottom two corners
                bottom = self.interpolate(field[sample_i0][sample_j1], field[sample_i1][sample_j1], horizontal_blend)
				
                # final blend
                res[i][j] = self.interpolate(top, bottom, vertical_blend)
                
        return res