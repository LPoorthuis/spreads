# -*- coding: utf-8 -*-

from RandomNoise import RandomNoise


class Map(object):

    def __init__(self, width, height, step):
        self.width = width
        self.height = height
        self.step = step
    
    def fill(self, image, color, field, lower, upper):
        step = self.step
        """Place pixel at pos=(x,y) on image, with color=(r,g,b)."""
        r,g,b = color
        for i in range(len(field)):
            for j in range(len(field[i])):
                if field[i][j] < upper and lower <= field[i][j]:
                    image.put("#%02x%02x%02x" % (r,g,b), (i*step, j*step))
                    for k in range(step):
                        for m in range(step):
                            image.put("#%02x%02x%02x" % (r,g,b), (i*step+k, j*step))
                            image.put("#%02x%02x%02x" % (r,g,b), (i*step, j*step+m))
                            image.put("#%02x%02x%02x" % (r,g,b), (i*step+k, j*step+m))
    
    def create(self):        
        i = 0
        
        r = RandomNoise()
        
        base = r.generateWhiteNoise(self.width, self.height)

        noise = r.generateSmoothNoise(base, 3)
        
        sTr = ''
        for i in noise:
            for j in i:
                sTr += "%1.3f " % j
            sTr += '\n'
        
        return noise