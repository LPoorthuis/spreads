# -*- coding: utf-8 -*-
"""
Created on Sat Dec 20 20:12:32 2014

@author: lukas
"""

from Civil import Civil
from Medics import Medic
from Zombie import Zombie
from Infected import Infected

import random
import math
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

class game(object):
    
    def __init__(self, width, height):
        self.field = [[0 for i in range(width)] for i in range(height)]
        self.medics = 1
        self.infected = 3
        self.zombies = 1
        self.civil = len(self.field) * len(self.field[0]) - self.infected - self.medics - self.zombies
        self.f = file('dists', 'w')
        self.count = 0

    def initGame(self):
        medics = self.medics
        zombies = self.zombies
        infected = self.infected         
        field = self.field
        

        for i in range(len(field)):
            for j in range(len(field[0])):
                field[i][j] = Civil(i, j, 0)
        
        while (medics > 0):
            x = random.randint(0, len(field) - 1)
            y = random.randint(0, len(field[0]) - 1)
            if type(field[x][y]) is Civil:
                field[x][y] = Medic(x, y, 0)
                medics -= 1

        while (zombies > 0):
            x = random.randint(0, len(field) - 1)
            y = random.randint(0, len(field[0]) - 1)
            if type(field[x][y]) is Civil:
                field[x][y] = Zombie(x, y, 0)
                zombies -= 1

        while (infected > 0):
            x = random.randint(0, len(field) - 1)
            y = random.randint(0, len(field[0]) - 1)
            if type(field[x][y]) is Civil:
                field[x][y] = Infected(x, y, 0)
                infected -= 1

    def zombiesExist(self):
        field = self.field
        
        for i in range(len(field)):
            for j in range(len(field[0])):
                if type(field[i][j]) is Zombie:
                    return True
        return False

    def change(self, status, x, y):
        field = self.field
        # print 'change to ' + str(status) 
        # change to Zombie (1)
        if status == 1:
            field[x][y] = Zombie(x, y, field[x][y].status)
        # change to Civil (2)
        if status == 2:
            field[x][y] = Civil(x, y, field[x][y].status)
        # change to Medic (3)
        if status == 3:
            field[x][y] = Medic(x, y, field[x][y].status)
        # change to Infected (4)
        if status == 4:
            field[x][y] = Infected(x, y, field[x][y].status)

    def influence(self, x, y, medic, zombie):
        field = self.field
        f = self.f
        
        radius = field[x][y].radius

        if x - radius - 1 < 0:
            horMin = 0
        else:
            horMin = x - radius - 1

        if x + radius + 1 > len(field):
            horMax = len(field)
        else:
            horMax = x + radius + 1

        if y - radius - 1 < 0:
            verMin = 0
        else:
            verMin = y - radius - 1

        if y + radius + 1 > len(field[0]):
            verMax = len(field[0])
        else:
            verMax = y + radius + 1

        # print 'x y'
        # print str(x) + ' ' + str(y)
        f.write(str(x) + ' ' + str(y) + '\n')
        f.write(str(horMin) + ' ' + str(horMax) + '\n')
        f.write(str(verMin) + ' ' + str(verMax) + '\n')
        for i in range(horMin, horMax):
            for j in range(verMin, verMax):
                # print str(i) + ' ' + str(j)
                dist = math.sqrt(math.sqrt((x - i) ** 2 + (y - j) ** 2))
                if dist**2 > radius:
                    continue
                value = radius - dist
                    
                f.write(str(i) + ' ' + str(j) + ': ' + str(dist) + ' --> effect ' + str(value) + '\n')

                # entity specific                     
                if zombie:                    
                    field[i][j].status += value
                
                if medic and type(field[i][j]) is Civil:
                    field[i][j].status += value * field[x][y].experience
                    field[i][j].resistance += 1
                
        f.write('\n')

    def turn(self):
        field = self.field
        
        for i in range(len(field)):
            for j in range(len(field[0])):
                self.change(field[i][j].turn(), i, j)
        
        # influencing
        for i in range(len(field)):
            for j in range(len(field[0])):
                if type(field[i][j]) is Medic:
                    self.influence(i, j, True, False)
                if type(field[i][j]) is Zombie:
                    self.influence(i, j, False, True)
                    
        #self.statistics()
        self.count += 1
        #self.toSTDOut()
        
    def toSTDOut(self):
        field = self.field
        
        for i in field:
            print i

    def spiel(self):      
        self.turn()
        self.toSTDOut()
        self.count += 1
        
    def createImg(self, arr, string):      
        tmp = np.asarray(arr)

        tmp = np.rot90(tmp)

        plt.imshow(tmp, interpolation='bilinear', cmap=cm.RdYlGn,
                        origin='lower', extent=[-100,100,-100,100],
                        vmax=abs(tmp).max(), vmin=-abs(tmp).max())

        plt.title(string)
        plt.savefig('statistics/'+string+str(self.count), dpi=48)
        #plt.show()

    def statistics(self):
        field = self.field
        
        zomArr = [[0 for i in range(len(field))] for i in range(len(field[0]))]
        civArrMed = [[0 for i in range(len(field))] for i in range(len(field[0]))]
        civArrZom = [[0 for i in range(len(field))] for i in range(len(field[0]))]
        medArr = [[0 for i in range(len(field))] for i in range(len(field[0]))]
        infArrMed = [[0 for i in range(len(field))] for i in range(len(field[0]))]
        infArrZom = [[0 for i in range(len(field))] for i in range(len(field[0]))]

        
        for i in range(len(field)):
            for j in range(len(field[i])):
                if type(field[i][j]) is Zombie: 
                    zomArr[i][j] = field[i][j].medified
                if type(field[i][j]) is Civil:
                    civArrZom[i][j] = field[i][j].zombified
                    civArrMed[i][j] = field[i][j].medified
                if type(field[i][j]) is Infected:
                    infArrMed[i][j] = field[i][j].zombified
                    infArrZom[i][j] = field[i][j].medified
                if type(field[i][j]) is Medic:
                    medArr[i][j] = field[i][j].experience
                    

        self.createImg(zomArr, 'zomArr')
        self.createImg(civArrMed, 'civArrMed')
        self.createImg(civArrZom, 'civArrZom')
        self.createImg(medArr, 'medArr')
        self.createImg(infArrMed, 'infArrMed')
        self.createImg(infArrZom, 'infArrZom')                    